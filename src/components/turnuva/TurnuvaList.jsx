import { useState, useEffect } from "react";
import { List } from "antd";
import axios from "axios";
import TurnuvaCard from "../ui/TurnuvaCard";


const TurnuvaList = ({ yil }) => {
  const [turnuvalar, setTurnuvalar] = useState([]);

  useEffect(() => {
    const turnuvalariGetir = async () => {
      try {
        const response = await axios.get(
            "http://localhost:8090/api/turnuva/getTurnuvaList?yil=" + yil.toString());
        if (response.status === 200) {
          setTurnuvalar(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    };
    turnuvalariGetir();
  }, [yil]);

  return (
    <div style={{ padding: "2rem" }}>
      <List
        grid={{ gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 2, xxl: 2 }}
        dataSource={turnuvalar}
        renderItem={(turnuva) => (
          <TurnuvaCard turnuva={turnuva} />
        )}
      />
    </div>
  );
};

export default TurnuvaList;