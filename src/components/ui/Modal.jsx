import { Modal, Input, Form, message } from 'antd';
import axios from 'axios';

const ModalComponent = ({ visible, onOk, onCancel, eslesmeId }) => {
  const [form] = Form.useForm();

  const onSubmit = async (values) => {
    try {
      const response = await axios.put("http://localhost:8090/api/eslesme/update", {
        id : eslesmeId,
        skor: values.skor
      });
      if (response.status === 200) {
        message.success('Skor güncellendi!');
        onOk();
      }
    } catch (error) {
      console.error(error);
      message.error('Skor güncellenirken hata oluştu.');
    }
  };

  return (
    <Modal
      title="Skoru Güncelle"
      visible={visible}
      onOk={() => form.submit()}
      onCancel={onCancel}
    >
      <Form form={form} onFinish={onSubmit}>
        <Form.Item
          name="skor"
          rules={[{ required: true, message: 'Lütfen skoru girin!' }]}
        >
          <Input placeholder="Skor" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalComponent;