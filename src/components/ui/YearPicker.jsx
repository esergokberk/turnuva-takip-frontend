import {useState,useEffect} from "react";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { Button } from "antd";

const YearPicker = ({ selectedYear, onYearChange }) => {
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    setIsDisabled(selectedYear <= new Date().getFullYear());
  }, [selectedYear]);
  
  const changeYear = (year) => {
    onYearChange(selectedYear + year);
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "2rem",
      }}
    >
      <Button
        onClick={() => changeYear(-1)}
        icon={<LeftOutlined />}
        disabled={isDisabled}
      />
      <span style={{ margin: "0 10px" }}>
        {selectedYear}
      </span>
      <Button onClick={() => changeYear(1)} icon={<RightOutlined />} />
    </div>
  );
};

export default YearPicker;