import React from "react";
import { List, Card, Row, Col, Typography, Button } from "antd";
import { CalendarOutlined, InfoCircleOutlined } from "@ant-design/icons";
import Link from 'next/link';


const { Text } = Typography;

const TurnuvaCard = ({ turnuva }) => {
  return (
    <List.Item>
       <Card
        title={
            <Link href={`/turnuva/${turnuva.id}`}>
            <Button type="primary">
              <InfoCircleOutlined /> Turnuva Detayları
            </Button>
          </Link>
        }
        style={{ borderRadius: "15px" }}
        headStyle={{ backgroundColor: "#f0f2f5" }}
      >
        <Row gutter={[16, 16]} justify="start">
          <Col span={24}>
            <Text>
              <CalendarOutlined /> <Text strong>Turnuva Tipi:</Text>{" "}
              {turnuva.turnuvaTipi.turnuvaTipi}
            </Text>
          </Col>
        </Row>
      </Card>
    </List.Item>
  );
};

export default TurnuvaCard;