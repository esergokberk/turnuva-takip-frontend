import React from "react";
import { Layout, Menu, Dropdown, Button, Space } from "antd";
import {
  UserOutlined,
  LoginOutlined,
  LogoutOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { clearUser } from "../../slices/authSlice";

const { Header } = Layout;

const Navbar = () => {
  const kullanici = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();
  const router = useRouter();

  const menu = (
    <Menu>
      <Menu.Item
        key="logout"
        icon={<LogoutOutlined />}
        onClick={() => {
          dispatch(clearUser());
          router.push("/");
        }}
      >
        Çıkış
      </Menu.Item>
    </Menu>
  );

  return (
    <Header style={{ padding: 0 }}>
      <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }}>
        <Menu.Item key="1" icon={<HomeOutlined />}>
          <Link href="/" legacyBehavior>
            <a>Ana Sayfa</a>
          </Link>
        </Menu.Item>
        {!kullanici && (
          <Menu.Item key="2">
            <Link href="/signup" legacyBehavior>
              <a>Kayıt Ol</a>
            </Link>
          </Menu.Item>
        )}
        <Menu.Item key="3" style={{ float: "right" }}>
          {kullanici ? (
            <Dropdown overlay={menu} trigger={["click"]}>
              <Space>
                <Button
                  type="text"
                  style={{ color: "white" }}
                  onClick={(e) => e.preventDefault()}
                  icon={<UserOutlined />}
                >
                  {`${kullanici.adSoyad}`}
                </Button>
              </Space>
            </Dropdown>
          ) : (
            <Link href="/login" legacyBehavior>
              <a>
                <LoginOutlined /> Giriş Yap
              </a>
            </Link>
          )}
        </Menu.Item>
        {kullanici && kullanici.rol && kullanici.rol.id === 1 && (
            <Menu.Item key="4">
                <Link href="/turnuvaekle" legacyBehavior>
                    <a>Turnuva Ekle</a>
                </Link>
            </Menu.Item>
        )}
      </Menu>
    </Header>
  );
};

export default Navbar;
