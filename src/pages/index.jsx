import { useSelector } from "react-redux";
import TurnuvaListPage from "./turnuvalar";


export default function Home() {
  const kullanici = useSelector((state) => state.auth.user);

  const renderContent = () => {
    if (kullanici) {
        return <TurnuvaListPage />;
      } 
  
    return <div></div>;
  };

  return <div>{renderContent()}</div>;
}
