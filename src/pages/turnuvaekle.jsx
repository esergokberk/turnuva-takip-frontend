import React, { useState, useEffect } from "react";
import {
  Button,
  Select,
  message,
  Form,
  InputNumber,
  Card,
  Typography,
} from "antd";
import axios from "axios";

const { Option } = Select;
const { Title } = Typography;

const TurnuvaEkle = () => {
  const [turnuvaTipleri, setTurnuvaTipleri] = useState([]);
  const [form] = Form.useForm();

  useEffect(() => {
    const getTurnuvaTipleri = async () => {
      try {
        const response = await axios.get(
          "http://localhost:8090/api/turnuva/getTurnuvaTipList"
        );
        setTurnuvaTipleri(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    getTurnuvaTipleri();
  }, []);

  const turnuvaKaydet = async (values) => {
    try {
      const response = await axios.post(
        "http://localhost:8090/api/turnuva/create",
        {
            turnuvaTipiId: values.turnuvaTipi,
            yil: values.turnuvaYili,
        }
      );
      if (response.status === 200) {
        form.resetFields();
        message.success("Turnuva başarıyla kaydedildi.");
      }
    } catch (error) {
      if(error.response.status === 400){
        const errorData = error.response.data;
        message.error(errorData.message);
    }
    else{
        message.error("Turnuva eklenirken hata oluştu.");
    }
    }
  };

  return (
    <div>
      <div style={{ maxWidth: "500px", margin: "0 auto", padding: "5rem 0" }}>
        <Card>
          <Title level={3}>Turnuva Ekle</Title>
          <Form form={form} onFinish={turnuvaKaydet}>
  <Form.Item
    name="turnuvaTipi"
    rules={[
      {
        required: true,
        message: "Lütfen turnuva tipini seçiniz!",
      },
    ]}
  >
    <Select placeholder="Turnuva Tipi">
      {turnuvaTipleri.map((item) => (
        <Option key={item.id} value={item.id}>
          {item.turnuvaTipi}
        </Option>
      ))}
    </Select>
  </Form.Item>

  <Form.Item name="turnuvaYili" label="Turnuva Yılı">
    <InputNumber />
  </Form.Item>

  <Form.Item>
    <Button type="primary" htmlType="submit">
      Kaydet
    </Button>
  </Form.Item>
</Form>
        </Card>
      </div>
    </div>
  );
};

export default TurnuvaEkle;
