import { Form, Input, Select, Button, Typography, Card, message } from "antd";
import {
  UserOutlined,
  UserSwitchOutlined,
  NumberOutlined,
} from "@ant-design/icons";
import { useState, useEffect } from "react";
import axios from "axios";

const { Option } = Select;
const { Title } = Typography;

const Signup = () => {
  const [form] = Form.useForm();
  const [kullaniciRol, setKullaniciRol] = useState([]);

  const kullaniciKaydet = async (values) => {
    const kullanici = {
      adSoyad: values.adSoyad,
      kullaniciAdi: values.kullaniciAdi,
      rolId: values.kullaniciRol,
      yas: values.yas,
    };

    try {
      const response = await axios.post(
        "http://localhost:8090/api/kullanici/create",
        kullanici
      );
      if (response.status === 200) {
        form.resetFields();
        message.success("Kayıt Başarılı");
      } else {
        throw new Error("Kayıt Başarısız");
      }
    } catch (err) {
      console.log(err);
      message.error("Kayıt Başarısız");
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [kullaniciRolResponse] = await Promise.all([
          axios.get("http://localhost:8090/api/kullanici-rol/list"),
        ]);
        setKullaniciRol(kullaniciRolResponse.data);
      } catch (err) {
        message.error("Bir hata oluştu, lütfen daha sonra tekrar deneyin");
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      <div style={{ maxWidth: "500px", margin: "0 auto", padding: "5rem 0" }}>
        <Card>
          <Title level={3}>Kayıt Ol</Title>
          <Form form={form} onFinish={kullaniciKaydet}>
            <Form.Item
              name="adSoyad"
              rules={[
                {
                  required: true,
                  message: "Lütfen adınızı ve soyadınızı giriniz!",
                },
              ]}
            >
              <Input prefix={<UserOutlined />} placeholder="Ad Soyad" />
            </Form.Item>
            <Form.Item
              name="kullaniciAdi"
              rules={[
                {
                  required: true,
                  message: "Lütfen kullanıcı adınızı giriniz!",
                },
              ]}
            >
              <Input prefix={<UserOutlined />} placeholder="Kullanıcı Adı" />
            </Form.Item>
            <Form.Item
              name="kullaniciRol"
              rules={[
                {
                  required: true,
                  message: "Lütfen kullanıcı rolünüzü seçiniz!",
                },
              ]}
            >
              <Select
                placeholder="Kullanıcı Rolü"
                prefix={<UserSwitchOutlined />}
              >
                {kullaniciRol.map((item) => (
                  <Option key={item.id} value={item.id}>
                    {item.rol}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="yas"
              rules={[
                {
                  required: true,
                  message: "Lütfen kullanıcı yaşı giriniz!",
                },
              ]}
            >
              <Input
                prefix={<NumberOutlined />}
                placeholder="Yaş"
                type="number"
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Kayıt Ol
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    </div>
  );
};

export default Signup;
