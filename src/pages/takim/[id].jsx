import { useState, useEffect } from "react";
import axios from "axios";
import { Table, Card, Form, Input, Button, message, Select } from "antd";
import { useSelector } from "react-redux";

const { Option } = Select;

const TurnuvaDetayPage = ({ id }) => {
  const [kadro, setKadro] = useState([]);
  const [oyuncuList, setOyuncuList] = useState([]);
  const [oyuncu, setOyuncu] = useState(null);
  const [form] = Form.useForm();

  const kullanici = useSelector((state) => state.auth.user);

  const rolIds = [1, 2, 3];

  const kadroGetir = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8090/api/takim/getTakimOyuncuList?takimId=" + id
      );
      if (response.status === 200) {
        const formattedKadroData = response.data.map((item) => ({
          id: item.id,
          adSoyad: item.oyuncu.adSoyad,
          numara: item.numara,
          yas: item.oyuncu.yas,
        }));
        setKadro(formattedKadroData);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    kadroGetir();
  }, [id]);

  const oyuncuEkle = async (values) => {
    try {
      const response = await axios.post(
        "http://localhost:8090/api/oyuncu/create",
        {
          takimId: id,
          oyuncuId: oyuncu,
          numara: values.numara,
        }
      );
      if (response.status === 200) {
        form.resetFields();
        message.success("Oyuncu başarıyla eklendi.");
        kadroGetir();
      }
    } catch (error) {
      if (error.response.status === 400) {
        const errorData = error.response.data;
        message.error(errorData.message);
      } else {
        message.error("Oyuncu eklenirken hata oluştu.");
      }
    }
  };

  const oyuncuSil = async (oyuncuId) => {
    try {
      const response = await axios.delete(
        "http://localhost:8090/api/oyuncu/delete?oyuncuId=" + oyuncuId
      );
      if (response.status === 200) {
        message.success("Oyuncu başarıyla silindi.");
        kadroGetir();
      }
    } catch (error) {
      message.error("Oyuncu silinirken hata oluştu.");
    }
  };

  useEffect(() => {
    const getOyuncuList = async () => {
      try {
        const response = await axios.get(
          "http://localhost:8090/api/kullanici/getKullanicilarByRolIds",
          {
            params: {
              rolIds: rolIds.join(","),
            },
          }
        );
        setOyuncuList(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    getOyuncuList();
  }, []);

  const columnsKadro = [
    {
      title: "Adı Soyadı",
      dataIndex: "adSoyad",
      key: "adSoyad",
    },
    {
      title: "Forma Numarası",
      dataIndex: "numara",
      key: "numara",
    },
    ...(kullanici && kullanici.rol && kullanici.rol.id === 1
      ? [
          {
            title: "Yaş",
            dataIndex: "yas",
            key: "yas",
          },
          {
            title: "Sil",
            dataIndex: "id",
            key: "sil",
            render: (id) => <Button onClick={() => oyuncuSil(id)}>Sil</Button>,
          },
        ]
      : []),
  ];

  return (
    <div style={{ padding: "2rem", minHeight: "100vh" }}>
      <h2 style={{ marginBottom: "2rem" }}>Takım Kadro</h2>
      {kullanici && kullanici.rol && kullanici.rol.id === 1 && (
        <Card style={{ marginBottom: "2rem" }}>
          <Form form={form} onFinish={oyuncuEkle}>
            <Form.Item
              name="oyuncu"
              rules={[{ required: true, message: "Lütfen oyuncu seçiniz!" }]}
            >
              <Select
                placeholder="Oyuncu"
                onChange={(value) => setOyuncu(value)}
              >
                {oyuncuList.map((oyuncu) => (
                  <Option key={oyuncu.id} value={oyuncu.id}>
                    {oyuncu.adSoyad}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="numara"
              rules={[{ required: true, message: "Lütfen numara giriniz!" }]}
            >
              <Input placeholder="Forma Numarası" />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Kaydet
              </Button>
            </Form.Item>
          </Form>
        </Card>
      )}
      <Card>
        <Table columns={columnsKadro} dataSource={kadro} />
      </Card>
    </div>
  );
};

export async function getServerSideProps({ params }) {
  const { id } = params;
  return {
    props: {
      id,
    },
  };
}

export default TurnuvaDetayPage;
