import Navbar from "@/components/ui/Navbar";
import "@/styles/globals.css";
import "antd/dist/antd.css";
import { Provider } from "react-redux";
import store from '../store'

export default function App({ Component, pageProps }) {
  return (
    <>
      <Provider store={store}>
        <Navbar />
        <Component {...pageProps} />
      </Provider>
    </>
  );
}