import { useState, useEffect } from "react";
import axios from "axios";
import {
  Table,
  Button,
  Card,
  Space,
  Form,
  Select,
  message,
  Input,
  Col,
  Row,
} from "antd";
import { RightOutlined } from "@ant-design/icons";
import Link from "next/link";
import { useSelector } from "react-redux";
import ModalComponent from "@/components/ui/Modal";

const { Option } = Select;

const TurnuvaDetayPage = ({ id }) => {
  const [puanDurumu, setPuanDurumu] = useState([]);
  const [eslesme, setEslesme] = useState([]);

  const [takimlar, setTakimlar] = useState([]);
  const [form] = Form.useForm();

  const [modalVisible, setModalVisible] = useState(false);
  const [guncelEslesmeId, setGuncelEslesmeId] = useState(null);

  const [sorumluList, setSorumluList] = useState([]);

  const [takimAdi, setTakimAdi] = useState("");
  const [sorumlu, setSorumlu] = useState(null);

  const kullanici = useSelector((state) => state.auth.user);

  const rolIds = [1, 2];

  useEffect(() => {
    const getSorumlulist = async () => {
      try {
        const response = await axios.get(
          "http://localhost:8090/api/kullanici/getKullanicilarByRolIds",
          {
            params: {
              rolIds: rolIds.join(","),
            },
          }
        );
        setSorumluList(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    getSorumlulist();
  }, []);

  const takimOlustur = async () => {
    try {
      const response = await axios.post(
        "http://localhost:8090/api/takim/create",
        {
          turnuvaId: id,
          takimAdi: takimAdi,
          takimSorumlusuId: sorumlu,
        }
      );
      if (response.status === 200) {
        message.success("Takım başarıyla oluşturuldu.");
        puanDurumunuGetir();
        getTakimlar();
        setTakimAdi("");
        setSorumlu(null);

      }
    } catch (error) {
      if (error.response.status === 400) {
        const errorData = error.response.data;
        message.error(errorData.message);
      } else {
        message.error("Takım oluşturma işlemi başarısız oldu.");
      }
    }
  };


  const getTakimlar = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8090/api/takim/getTurnuvaTakimList?turnuvaId=" + id
      );
      setTakimlar(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getTakimlar();
  }, [id]);

  const eslesmeOlustur = async (values) => {
    try {
      const response = await axios.post(
        "http://localhost:8090/api/eslesme/create",
        {
          takim1Id: values.takim1,
          takim2Id: values.takim2,
          skor: "",
        }
      );
      if (response.status === 200) {
        message.success("Eşleşme başarıyla oluşturuldu.");
        eslesmeleriGetir();
      }
    } catch (error) {
      console.error(error);
      message.error("Eşleşme oluşturma işlemi başarısız oldu.");
    }
  };

  const puanDurumunuGetir = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8090/api/takim-puan/siralama?turnuvaId=" + id
      );
      if (response.status === 200) {
        const formattedPuanData = response.data.map((item) => ({
          id: item.takim.id,
          takimAdi: item.takim.takimAdi,
          puan: item.puan,
        }));
        setPuanDurumu(formattedPuanData);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    puanDurumunuGetir();
  }, [id]);

  const eslesmeleriGetir = async () => {
    try {
      const response = await axios.get(
        "http://localhost:8090/api/eslesme/getTurnuvaEslesmeList?turnuvaId=" +
          id
      );
      if (response.status === 200) {
        const formattedEslesmeData = response.data.map((item) => ({
          id: item.id,
          takim1: item.takim1.takimAdi,
          takim2: item.takim2.takimAdi,
          skor: item.skor,
        }));
        setEslesme(formattedEslesmeData);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    eslesmeleriGetir();
  }, [id]);

  const handleOk = () => {
    setModalVisible(false);
    eslesmeleriGetir();
    puanDurumunuGetir();
  };

  const handleCancel = () => {
    setModalVisible(false);
  };

  const columnsPuan = [
    {
      title: "Takım Adı",
      dataIndex: "takimAdi",
      key: "takimAdi",
    },
    {
      title: "Puan",
      dataIndex: "puan",
      key: "puan",
    },
    {
      title: "Detay",
      key: "detail",
      render: (record) => (
        <Link href={`/takim/${record.id}`}>
          <Button type="primary" icon={<RightOutlined />}>
            Detay
          </Button>
        </Link>
      ),
    },
  ];

  const columnsEslesme = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
      hidden: true,
    },
    {
      title: "Takım 1",
      dataIndex: "takim1",
      key: "takim1",
    },
    {
      title: "Takım 2",
      dataIndex: "takim2",
      key: "takim2",
    },
    {
      title: "Skor",
      dataIndex: "skor",
      key: "skor",
    },
    ...(kullanici && kullanici.rol && kullanici.rol.id === 1
      ? [
          {
            title: "Güncelle",
            key: "update",
            render: (record) =>
              kullanici &&
              kullanici.rol &&
              kullanici.rol.id === 1 && (
                <Button
                  type="primary"
                  onClick={() => {
                    setGuncelEslesmeId(record.id);
                    setModalVisible(true);
                  }}
                >
                  Güncelle
                </Button>
              ),
          },
        ]
      : []),
  ];

  return (
    <div style={{ padding: "2rem", minHeight: "100vh" }}>
      <Space direction="vertical" size="large" style={{ width: "100%" }}>
        <Row gutter={[16, 16]}>
          {kullanici && kullanici.rol && kullanici.rol.id === 1 && (
            <Col xs={24} md={12}>
              <Row gutter={[16, 16]}>
                <Col xs={24}>
                  <Card title="Takım Oluştur">
                    <Form onFinish={takimOlustur}>
                      <Form.Item>
                        <Input
                          placeholder="Takım Adı"
                          value={takimAdi}
                          onChange={(e) => setTakimAdi(e.target.value)}
                        />
                      </Form.Item>
                      <Form.Item
                        name="sorumlu"
                        rules={[
                          {
                            required: true,
                            message: "Lütfen sorumlu kişiyi seçin!",
                          },
                        ]}
                      >
                        <Select
                          placeholder="Takım Sorumlusu"
                          onChange={(value) => setSorumlu(value)}
                        >
                          {sorumluList.map((kullanici) => (
                            <Option key={kullanici.id} value={kullanici.id}>
                              {kullanici.adSoyad}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          Kaydet
                        </Button>
                      </Form.Item>
                    </Form>
                  </Card>
                </Col>
                <Col xs={24}>
                  <Card title="Eşleşme Oluştur">
                    <Form form={form} onFinish={eslesmeOlustur}>
                      <Form.Item
                        name="takim1"
                        rules={[
                          {
                            required: true,
                            message: "Lütfen birinci takımı seçin!",
                          },
                        ]}
                      >
                        <Select placeholder="Takım 1">
                          {takimlar.map((takim) => (
                            <Option key={takim.id} value={takim.id}>
                              {takim.takimAdi}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="takim2"
                        rules={[
                          {
                            required: true,
                            message: "Lütfen ikinci takımı seçin!",
                          },
                        ]}
                      >
                        <Select placeholder="Takım 2">
                          {takimlar.map((takim) => (
                            <Option key={takim.id} value={takim.id}>
                              {takim.takimAdi}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          Eşleşme Oluştur
                        </Button>
                      </Form.Item>
                    </Form>
                  </Card>
                </Col>
              </Row>
            </Col>
          )}
          <Col
            xs={24}
            md={kullanici && kullanici.rol && kullanici.rol.id === 1 ? 12 : 24}
          >
            <Row gutter={[16, 16]}>
              <Col xs={24}>
                <Card title="Puan Durumu">
                  <Table
                    columns={columnsPuan}
                    dataSource={puanDurumu}
                    rowKey="takimAdi"
                  />
                </Card>
              </Col>
              <Col xs={24}>
                <Card title="Maçlar">
                  <Table
                    columns={columnsEslesme}
                    dataSource={eslesme}
                    rowKey="id"
                  />
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Space>
      <ModalComponent
        visible={modalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        eslesmeId={guncelEslesmeId}
      />
    </div>
  );
};

export async function getServerSideProps({ params }) {
  const { id } = params;
  return {
    props: {
      id,
    },
  };
}

export default TurnuvaDetayPage;
