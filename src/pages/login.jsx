import { Button, Card, Form, Input, Typography, message } from "antd";
import { UserOutlined } from "@ant-design/icons";
import React from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setUser } from "../slices/authSlice";
import { useRouter } from "next/router";

const { Title } = Typography;

const Login = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const kullaniciGiris = async (value) => {
    try {
      const response = await axios.post(
        "http://localhost:8090/api/kullanici/login?kullaniciAdi=" + value.kullaniciAdi);
      if (response.status === 200) {
        const kullanici = response.data;
        dispatch(setUser(kullanici));
        message.success("Giriş Başarılı");
        router.push("/");
      } else {
        throw new Error("Giriş Başarısız");
      }
    } catch (error) {
      console.error(error);
      message.error("Giriş Başarısız");
    }
  };

  return (
    <div>
      <div style={{ maxWidth: "500px", margin: "0 auto", padding: "5rem 0" }}>
        <Card>
          <Title level={3}>Giriş Yap</Title>
          <Form onFinish={kullaniciGiris}>
            <Form.Item
              name="kullaniciAdi"
              rules={[
                {
                  required: true,
                  message: "Lütfen Kullanıcı Adınızı giriniz!",
                },
              ]}
            >
              <Input prefix={<UserOutlined />} placeholder="Kullanıcı Adı" />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Giriş Yap
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    </div>
  );
};

export default Login;
