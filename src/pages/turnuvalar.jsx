import React, { useState } from "react";
import YearPicker from "../components/ui/YearPicker";
import TurnuvaList from "../components/turnuva/TurnuvaList";

const TurnuvaListPage = () => {
  const simdikiYil = new Date().getFullYear();
  const [seciliYil, setSeciliYil] = useState(simdikiYil);

  return (
    <div>
      <YearPicker selectedYear={seciliYil} onYearChange={setSeciliYil} />
      <TurnuvaList yil={seciliYil} />
    </div>
  );
};

export default TurnuvaListPage;